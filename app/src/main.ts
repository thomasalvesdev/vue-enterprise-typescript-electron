import Vue from 'vue'
import App from './app.vue'
import router from '@router/index.ts'
import store from '@state/store.ts'
import '@components/_globals.ts'

// Don't warn about using the dev version of Vue in development
Vue.config.productionTip = process.env.NODE_ENV === 'production'

const app: Vue = new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')

// If running inside Cypress
if ((window as any).Cypress) {
  // Attach the app to the window, which can be useful
  // for manually setting state in Cypress commands
  // such as `cy.logIn()`
  // eslint-disable-next-line
  ;(window as any).__app__ = app
}
