import BaseButton from './_base-button.vue'

describe('@components/_base-button', () => {
  it('renders its content', () => {
    const slotContent = '<span>foo</span>'
    const { element } = mount(
      {
        render(h) {
          return h('BaseButton', [h('span', ['foo'])])
        },
      },
      {
        slots: {
          default: slotContent,
        },
      }
    )
    console.log(element.innerHTML)
    expect(element.innerHTML).toContain(slotContent)
  })
})
