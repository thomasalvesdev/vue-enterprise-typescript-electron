import NavBarRoutes from './nav-bar-routes'

const mountRoutes = options => {
  return mount(
    {
      components: { NavBarRoutes },
      render(h) {
        return h('ul', [
          h('NavBarRoutes', {
            attrs: {
              routes: [
                {
                  name: 'aaa',
                  title: 'bbb',
                },
              ],
            },
          }),
        ])
      },
    },
    {
      stubs: {
        BaseLink: {
          functional: true,
          render(h, { slots }) {
            return h('a', slots().default)
          },
        },
        ...options.stubs,
      },
      ...options,
    }
  )
}

describe('@components/nav-bar-routes', () => {
  it('correctly renders routes with text titles', () => {
    const { element } = mountRoutes({
      propsData: {
        routes: [
          {
            name: 'aaa',
            title: 'bbb',
          },
        ],
      },
    })
    expect(element.textContent).toEqual('bbb')
  })

  it('correctly renders routes with function titles', () => {
    const { element } = mountRoutes({
      propsData: {
        routes: [
          {
            name: 'aaa',
            title: () => 'bbb',
          },
        ],
      },
    })
    expect(element.textContent).toEqual('bbb')
  })
})
