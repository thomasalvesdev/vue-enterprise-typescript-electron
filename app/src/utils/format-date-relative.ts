// https://date-fns.org/docs/parse
const parseDate = require('date-fns/parse')
// https://date-fns.org/docs/distanceInWords
const distanceInWords = require('date-fns/distance_in_words')
// https://date-fns.org/docs/isToday
const isToday = require('date-fns/is_today')

export default function formatDateRelative(fromDate, toDate = new Date()) {
  fromDate = parseDate(fromDate)
  toDate = parseDate(toDate)
  return distanceInWords(fromDate, toDate) + (isToday(toDate) ? ' ago' : '')
}
