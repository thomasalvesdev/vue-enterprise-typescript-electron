// https://date-fns.org/docs/parse
const parseDate = require('date-fns/parse')
// https://date-fns.org/docs/format
const format = require('date-fns/format')

export default function formatDate(date) {
  date = parseDate(date)
  return format(date, 'MMM Do, YYYY')
}
