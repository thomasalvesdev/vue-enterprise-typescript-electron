const path = require('path')

const appDir = '../app'

function resolveSrc(_path) {
  return path.resolve(appDir, _path)
}

const aliases = {
  '@src': 'src',
  '@router': 'src/router',
  '@views': 'src/router/views',
  '@layouts': 'src/router/layouts',
  '@components': 'src/components',
  '@assets': 'src/assets',
  '@utils': 'src/utils',
  '@state': 'src/state',
  '@design': 'src/design/index.scss',
}

let newConfig = {
  resolve: {
    alias: {}
  }
}

for (const path in aliases) {
  newConfig.resolve.alias[path] = resolveSrc(aliases[path])
}

module.exports = newConfig